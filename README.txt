CFFI-wrapper - A simple but flexible module for creating object-oriented, pythonic CFFI wrappers.  

Just copy cffiwrap.py to your CFFI project. See the tests for some examples.

Documentation is here: http://cffiwrap.readthedocs.org/

Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
All rights reserved.

Licensed under Apache License, Version 2.0.
See LICENSE.txt for licensing details.
