.. cffiwrap documentation master file, created by
   sphinx-quickstart on Sun Sep  8 23:42:47 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cffiwrap's documentation!
====================================

CFFIwrap is a set of convenience functions and wrapper classes designed to
make writing CFFI modules less tedious. The source tree contains a single
python module called cffiwrap.py. Simply copy this file in to your CFFI
project and check out the module docs which contain several examples. For more
examples take a look in the tests directory.

This project is licensed under the Apache License, version 2.0.

.. toctree::
   :maxdepth: 4

Module documentation
====================
.. automodule:: cffiwrap
    :members:
    :undoc-members:
    :show-inheritance:

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

